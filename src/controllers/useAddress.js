import { reactive } from "@vue/composition-api";
import api from "@/controllers/api";

export const useAddress = () => {
  // payments
  const province = reactive({
    isLoading: false,
    isFailed: false,
    list: []
  });

  const fetchProvince = async () => {
    province.isLoading = true;
    const response = await api.fetchData("/address/province");
    if (response.status === 200) {
      province.list = response.data.data;
      province.isLoading = false;
    } else {
      province.list = [];
      province.isLoading = false;
    }
  };

  //city
  const city = reactive({
    isLoading: false,
    isFailed: false,
    list: []
  });

  const fetchCity = async provinceId => {
    city.isLoading = true;
    const response = await api.fetchData(`/address/city/${provinceId}`);
    if (response.status === 200) {
      city.list = response.data.data;
      city.isLoading = false;
    } else {
      city.list = [];
      city.isLoading = false;
    }
  };

  //district
  const district = reactive({
    isLoading: false,
    isFailed: false,
    list: []
  });

  const fetchDistrict = async cityId => {
    district.isLoading = true;
    const response = await api.fetchData(`/address/district/${cityId}`);
    if (response.status === 200) {
      district.list = response.data.data;
      district.isLoading = false;
    } else {
      district.list = [];
      district.isLoading = false;
    }
  };

  //village
  const village = reactive({
    isLoading: false,
    isFailed: false,
    list: []
  });

  const fetchVillage = async districtId => {
    village.isLoading = true;
    const response = await api.fetchData(`/address/village/${districtId}`);
    if (response.status === 200) {
      village.list = response.data.data;
      village.isLoading = false;
    } else {
      village.list = [];
      village.isLoading = false;
    }
  };

  return {
    province,
    fetchProvince,
    city,
    fetchCity,
    district,
    fetchDistrict,
    village,
    fetchVillage
  };
};
