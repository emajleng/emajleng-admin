import Vue from "vue";
import * as GmapVue from "gmap-vue";

Vue.use(GmapVue, {
  load: {
    key: "AIzaSyDjBDbbO853-19a_gs0lLuy7IWbjEOTjUo",
    installComponents: true,
    libraries: "places"
  }
});

export default GmapVue;
