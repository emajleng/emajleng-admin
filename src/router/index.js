import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  { path: "/", redirect: { name: "login" } },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/auth/index"),
    meta: {
      title: "Login Admin | Majalengka",
      layout: "default",
      requiresVisitor: true
    }
  },

  {
    path: "/dashboard",
    name: "dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/dashboard/index"),
    meta: {
      title: "Dashboard | Majalengka Admin",
      layout: "admin",
      requiresAuth: true
    }
  },

  {
    path: "/membership",
    name: "membership",
    component: () =>
      import(/* webpackChunkName: "membership" */ "../views/membership/index"),
    meta: {
      title: "Membership | Majalengka Admin",
      layout: "admin",
      requiresAuth: true
    }
  },

  {
    path: "/merchants",
    name: "merchants",
    component: () =>
      import(/* webpackChunkName: "merchants" */ "../views/merchants/index"),
    meta: {
      title: "Merchants | Majalengka Admin",
      layout: "admin",
      requiresAuth: true
    }
  },

  {
    path: "/category",
    name: "category",
    component: () =>
      import(/* webpackChunkName: "category" */ "../views/category/index"),
    meta: {
      title: "Category | Majalengka Admin",
      layout: "admin",
      requiresAuth: true
    }
  },

  {
    path: "/product",
    name: "product",
    component: () =>
      import(/* webpackChunkName: "product" */ "../views/product/index"),
    meta: {
      title: "Product | Majalengka Admin",
      layout: "admin",
      requiresAuth: true
    }
  },

  {
    name: "transaction",
    path: "/transaction",
    component: () => import(/* webpackChunkName: "transaction" */ "../views"),
    redirect: "/transaction/list",
    children: [
      {
        path: "product",
        name: "transaction-product",
        component: () =>
          import(
            /* webpackChunkName: "transaction" */ "../views/transaction/product"
          ),
        meta: {
          title: "Transaction List | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        path: "list",
        name: "transaction-list",
        component: () =>
          import(
            /* webpackChunkName: "transaction" */ "../views/transaction/list"
          ),
        meta: {
          title: "Transaction List | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        path: "top-up",
        name: "transaction-topup",
        component: () =>
          import(
            /* webpackChunkName: "transaction" */ "../views/transaction/top-up"
          ),
        meta: {
          title: "Top up Transaction | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        path: "withdraw",
        name: "transaction-withdraw",
        component: () =>
          import(
            /* webpackChunkName: "transaction" */ "../views/transaction/withdraw"
          ),
        meta: {
          title: "Withdraw | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      }
    ]
  },

  {
    name: "provider",
    path: "/provider",
    component: () => import(/* webpackChunkName: "provider" */ "../views"),
    redirect: "/provider/index",
    children: [
      {
        name: "provider-index",
        path: "index",
        component: () =>
          import(
            /* webpackChunkName: "provider-index" */ "../views/provider/index"
          ),
        meta: {
          title: "Provider | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        name: "provider-detail",
        path: "detail/:provider_id",
        component: () =>
          import(
            /* webpackChunkName: "provider-detail" */ "../views/provider/detail"
          ),
        meta: {
          title: "Provider Detail | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      }
    ]
  },

  {
    name: "news",
    path: "/news",
    component: () => import(/* webpackChunkName: "news" */ "../views"),
    redirect: "/news/index",
    children: [
      {
        name: "news-index",
        path: "index",
        component: () =>
          import(/* webpackChunkName: "news-index" */ "../views/news/index"),
        meta: {
          title: "News | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        name: "news-add",
        path: "add",
        component: () =>
          import(/* webpackChunkName: "news-add" */ "../views/news/add"),
        meta: {
          title: "News Add | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        name: "news-edit",
        path: "news/:id",
        component: () =>
          import(/* webpackChunkName: "news-detail" */ "../views/news/edit"),
        meta: {
          title: "News Edit | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      }
    ]
  },

  {
    name: "settings",
    path: "/settings",
    component: () => import(/* webpackChunkName: "settings" */ "../views"),
    redirect: "/settings/banner",
    children: [
      {
        name: "setting-banner",
        path: "banner",
        component: () =>
          import(
            /* webpackChunkName: "setting-banner" */ "../views/settings/banner/index"
          ),
        meta: {
          title: "Setting | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },

      {
        name: "setting-menu",
        path: "menu",
        component: () =>
          import(
            /* webpackChunkName: "setting-menu" */ "../views/settings/menu/index"
          ),
        meta: {
          title: "Setting | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },

      {
        name: "setting-payment",
        path: "payment",
        component: () =>
          import(
            /* webpackChunkName: "setting-payment" */ "../views/settings/payment/index"
          ),
        meta: {
          title: "Setting | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },

      {
        name: "setting-courier",
        path: "courier",
        component: () =>
          import(
            /* webpackChunkName: "setting-courier" */ "../views/settings/courier/index"
          ),
        meta: {
          title: "Setting | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      },
      {
        name: "setting-admin-fee",
        path: "admin-fee",
        component: () =>
          import(
            /* webpackChunkName: "setting-admin-fee" */ "../views/settings/admin-fee/index"
          ),
        meta: {
          title: "Setting | Majalengka Admin",
          layout: "admin",
          requiresAuth: true
        }
      }
    ]
  }
];

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  routes
});

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};
